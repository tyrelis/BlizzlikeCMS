<footer>
    <div class="footer-border"></div>
    <div class="container">
        <div class="footer-content">
            <img src="img/warning1.gif">
            <img src="img/warning2.png" class="warning2">
        </div>
        <div class="footer-copyright"><?php echo $servername; ?> © <?php echo date("Y"); ?><br />Powered by a <a
                    href="https://gitlab.com/Delusional/BlizzlikeCMS" target="_blank">Blizzlike CMS</a></div>
    </div>
</footer>

<script src="https://ajax.googleapis.com/ajax/libs/jquery/3.2.1/jquery.min.js"></script>
<script src="includes/featherlight/featherlight.min.js"></script>
<script src="js/bootstrap.min.js"></script>
<script src="js/site.js"></script>
</body>
</html>