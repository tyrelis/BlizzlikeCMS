<div class="admin_front">
    <h1>Administration panel</h1>
    <a href="logout.php" class="admin_logout">Logout</a>
    <h3>Choose from the menu below:</h3>
    <ul>
        <li><span class="glyphicon glyphicon-user"></span> <a href="admin.php?page=edit_accounts">Edit accounts</a></li>
        <li><span class="glyphicon glyphicon-pencil"></span> <a href="admin.php?page=edit_posts">Edit posts</a></li>
        <li><span class="glyphicon glyphicon-picture"></span> <a href="admin.php?page=edit_gallery">Edit gallery</a></li>
        <li><span class="glyphicon glyphicon-flag"></span> <a href="admin.php?page=edit_gamemasters">Edit list of gamemasters</a></li>
        <li><span class="glyphicon glyphicon-cog"></span> <a href="admin.php?page=edit_settings">Edit website settings</a></li>
        <li><span class="glyphicon glyphicon-eye-open"></span> <a href="admin.php?page=edit_theme">Edit website theme</a></li>
    </ul>
</div>